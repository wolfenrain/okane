import 'dart:io';

import 'package:okane/template_builder/template_builder.dart';

void main() {
  final output = [
    '''
# Okane Document Reference

### okane.yaml

```yaml
templates_dir: ./templates # Directory where the templates are

defaults:
  <template_name>:
    # Default fields for specific template can be set like this.

# Fields on the root level will be applied to all templates.
<field_name>: <field_value>

theme:
  # While not "standard", templates often use the theme field for theming data.
```

### <template_file>.yaml

A template file is used for defining the structure. You can split up your template into partial files.
A partial file is the same as a template file with the added benefit that it can be reused. By using 
the `load` method you can load your partials where you need them.

```yaml
layout: # Each template file requires the layout field, and it should always be an array.
- # Your layout.

# Any required field for this template can be defined like this:
<field_name>: null # Setting it to null will tell okane that it is a required field

# Optional fields don't have to be defined but to make it easy you can add them commented out:
# <field_name>: optional
```

**NOTE**: Only the root level files in your template directory will be considered template files, so 
any partials you might have should be placed inside nested directories.

### <input_file>.yaml

```yaml
okane:
  template: <template_name> # Optional template, if missing the "--template" parameter is required on the command line.

# Fields required for the template are on the root level.
<field_name>: <field_value>
```

    '''
  ];

  output.add('## Methods\n');
  output.add('The following methods are supported by `okane`:\n');
  for (final method in TemplateBuilder.methods) {
    output.add('The `${method.name}` method: \n');
    output.add('```yaml');
    output.add(method.example);
    output.add('```\n');
  }

  output.add('## Widgets\n');
  output.add('The following widgets are supported by `okane`:\n');
  for (final widget in TemplateBuilder.widgets) {
    output.add('The `${widget.name}` widget: \n');
    output.add('```yaml');
    output.add('somewhere:\n  ${widget.name}:');
    output.add('```\n');
  }

  File.fromUri(Uri.parse('./okane_document_reference.md')).writeAsStringSync(
    output.join('\n'),
  );
}
