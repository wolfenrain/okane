import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:okane/config.dart';
import 'package:okane/utils/required_fields.dart';

import 'src/generate.dart';

void main(List<String> args) {
  final config = Config(Directory.current.uri.resolve('okane.yaml'));

  requiredFields(
    [
      'templates_dir',
    ],
    config,
    message: 'The following fields are required in ${config.path}:',
  );

  CommandRunner(
    'okane',
    'Manage invoices and quotations easily',
  )
    ..addCommand(Generate(config))
    ..run(args);
}
