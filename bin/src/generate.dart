import 'dart:io';

import 'package:okane/config.dart';
import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/utils/load_yaml.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

import 'base_command.dart';

class Generate extends BaseCommand {
  Generate(Config config) : super(config) {
    argParser.addOption(
      'template',
      abbr: 't',
      allowed: config.templates,
    );
  }

  @override
  String get description => 'generate';

  @override
  String get name => 'generate';

  @override
  Future<void> run() async {
    if (argResults!.rest.isEmpty) {
      print('Missing input file');
      exit(1);
    }
    String? output;
    if (argResults!.rest.length >= 2) {
      output = argResults!.rest[1];
    }
    print('Reading input file');
    final inputPath = config.path.resolve(argResults!.rest.first);
    final input = openYaml(inputPath);

    final template = argResults!['template'] ?? input['okane']?['template'];
    if (template == null) {
      print(
        'Template reference is required, either provide it in $inputPath or pass the "--template" parameter',
      );
      exit(1);
    }
    print('Using template: $template');
    input.remove('okane');

    final defaults = config.source.remove('defaults');
    final templateDefaults = defaults.remove(template) ?? {};
    final templatePath = config.templateDir.resolve('./$template.yaml');
    final templateData = openYaml(
      config.templateDir.resolve('./$template.yaml'),
    );
    final Map<String, dynamic> data = {
      ...templateData,
      ...config.source,
      ...templateDefaults,
      ...input,
    };

    // Prepare theme
    data['theme'] ??= {};

    // Prepare data
    final today = DateTime.now();
    String p(int d) => '$d'.padLeft(2, '0');
    data['unique_id'] ??=
        '${today.year}${p(today.month)}${p(today.day)}${p(today.hour)}';

    final fileName = inputPath.pathSegments.last.split('.').first;
    output ??= './output/${template}_${fileName}_${data['unique_id']}.pdf';

    data['hourly_rate'] ??= 0;
    data['discount'] ??= 0;
    data['vat'] ??= 0;
    data['valuta'] ??= 'EUR';
    data['tasks'] ??= [];
    data['tasks'].forEach((task) {
      task['hours'] ??= 0;
      task['hourly_rate'] ??= data['hourly_rate'];
      task['total_price'] ??= task['hours'] * task['hourly_rate'];
    });

    data['sub_total'] = data['tasks'].fold<double>(
      0.0,
      (double p, e) => p + e['total_price'],
    );
    data['discount_price'] = data['sub_total'] / 100 * data['discount'];

    data['vat_price'] =
        (data['sub_total'] - data['discount_price']) / 100 * data['vat'] ?? 0;
    data['total_price'] =
        ((data['sub_total'] - data['discount_price']) + data['vat_price']);

    final layoutBuilder = TemplateBuilder(templatePath, templateData, data);

    final pdf = Document();

    print('Building PDF');
    pdf.addPage(
      MultiPage(
        pageFormat: PdfPageFormat.a4,
        pageTheme: PageTheme(
          theme: ThemeData(
            defaultTextStyle: TextStyle(fontSize: 10),
          ),
        ),
        build: layoutBuilder.build,
      ),
    );

    print('Generating PDF');
    final file = File(output)..createSync(recursive: true);
    await file.writeAsBytes(await pdf.save());
    print('PDF generated');
  }
}
