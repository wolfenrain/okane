import 'package:args/command_runner.dart';
import 'package:okane/config.dart';

abstract class BaseCommand extends Command {
  BaseCommand(this.config);

  final Config config;
}
