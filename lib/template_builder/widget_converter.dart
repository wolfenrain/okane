import 'package:okane/template_builder/template_builder.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

export 'converters/column_converter.dart';
export 'converters/container_converter.dart';
export 'converters/divider_converter.dart';
export 'converters/expanded_converter.dart';
export 'converters/padding_converter.dart';
export 'converters/row_converter.dart';
export 'converters/sized_box_converter.dart';
export 'converters/table_converter.dart';
export 'converters/table_row_converter.dart';
export 'converters/text_converter.dart';
export 'converters/text_style_converter.dart';

abstract class WidgetConverter {
  String get name;

  Widget? convert(
    TemplateBuilder template,
    dynamic information,
    Map<String, dynamic>? data,
  );

  Border border(
    TemplateBuilder template,
    dynamic information, [
    Map<String, dynamic> data = const {},
  ]) {
    return Border(
      top: borderSide(template, information, data['top']),
      left: borderSide(template, information, data['left']),
      right: borderSide(template, information, data['right']),
      bottom: borderSide(template, information, data['bottom']),
    );
  }

  BoxDecoration boxDecoration(
    TemplateBuilder template,
    dynamic information,
    Map<String, dynamic> data,
  ) {
    return BoxDecoration(border: border(template, information, data['border']));
  }

  FontWeight? fontWeight(String? fontWeight) {
    return <String, FontWeight>{
      'normal': FontWeight.normal,
      'bold': FontWeight.bold,
    }[fontWeight];
  }

  TextAlign? textAlign(String? textAlign) {
    return <String, TextAlign>{
      'left': TextAlign.left,
      'center': TextAlign.center,
      'right': TextAlign.right,
    }[textAlign];
  }

  MainAxisAlignment mainAxisAlignment(String? mainAxisAlignment) {
    if (mainAxisAlignment == null) return MainAxisAlignment.start;
    final map = <String, MainAxisAlignment>{
      'start': MainAxisAlignment.start,
      'center': MainAxisAlignment.center,
      'end': MainAxisAlignment.end,
      'space_around': MainAxisAlignment.spaceAround,
      'space_between': MainAxisAlignment.spaceBetween,
      'space_evenly': MainAxisAlignment.spaceEvenly,
    };
    if (!map.containsKey(mainAxisAlignment)) {
      throw UnsupportedError('main_axis_alignment: $mainAxisAlignment');
    }
    return map[mainAxisAlignment]!;
  }

  CrossAxisAlignment crossAxisAlignment(String? crossAxisAlignment) {
    if (crossAxisAlignment == null) return CrossAxisAlignment.center;
    final map = <String, CrossAxisAlignment>{
      'start': CrossAxisAlignment.start,
      'center': CrossAxisAlignment.center,
      'end': CrossAxisAlignment.end,
      'stretch': CrossAxisAlignment.stretch,
    };
    if (!map.containsKey(crossAxisAlignment)) {
      throw UnsupportedError('cross_axis_alignment: $crossAxisAlignment');
    }
    return map[crossAxisAlignment]!;
  }

  Map<int, TableColumnWidth> tableColumnWidth(Map<String, dynamic> data) {
    final item = data.entries.first;
    if (item.key == 'intrinsic_column_width') {
      return {
        item.value['index']: IntrinsicColumnWidth(
          flex: item.value['flex']?.toDouble(),
        )
      };
    }
    throw Exception('Unknown table column: ${item.key}');
  }

  BorderSide borderSide(
    TemplateBuilder template,
    dynamic information,
    Map<String, dynamic>? data,
  ) {
    if (data == null) {
      return BorderSide.none;
    }

    return BorderSide(
      width: data['width']?.toDouble() ?? 1.0,
      color: template.color(information, data['color']) ?? PdfColors.black,
    );
  }
}
