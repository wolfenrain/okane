import 'dart:io';

import 'package:intl/intl.dart';
import 'package:okane/template_builder/method.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:okane/utils/get.dart';
import 'package:okane/utils/required_fields.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

typedef MethodBuilder = List<Widget?> Function(
  TemplateBuilder template,
  dynamic information,
  List<String> arguments,
  dynamic layoutData,
);

class TemplateBuilder {
  TemplateBuilder(
    this.uri,
    Map<String, dynamic> data,
    Map<String, dynamic> information, {
    this.root = '',
  }) {
    _layout = data.remove('layout');
    // Check any mising values.
    final fields = _requiredFields('', data);
    rootInformation = data..addAll(information);

    requiredFields(
      fields,
      rootInformation,
      message:
          'The following fields are required in $uri${root.isNotEmpty ? ' but were not found in "$root":' : ''}',
    );
  }

  late final Map<String, dynamic> rootInformation;

  late final dynamic _layout;

  final Uri uri;

  final String root;

  late Context context;

  static final List<Method> methods = [
    IfMethod(),
    ForMethod(),
    LoadMethod(),
  ];

  static final List<WidgetConverter> widgets = [
    ColumnConverter(),
    ContainerConverter(),
    DividerConverter(),
    ExpandedConverter(),
    ExpandedConverter(),
    PaddingConverter(),
    RowConverter(),
    SizedBoxConverter(),
    TableConverter(),
    TableRowConverter(),
    TextConverter(),
    TextStyleConverter(),
  ];

  static void registerMethod(Method method) {
    if (methods.any((e) => e.name == method.name)) {
      throw Exception(
        'There is already a method with the name "${method.name}"',
      );
    }
    methods.add(method);
  }

  static void registerWidget(WidgetConverter converter) {
    if (widgets.any((e) => e.name == converter.name)) {
      throw Exception(
        'There is already a converter with the name "${converter.name}"',
      );
    }
    widgets.add(converter);
  }

  dynamic raw(dynamic information, String? key) {
    if (key == null) {
      return information;
    }
    if (key.startsWith(r'.')) {
      return get(rootInformation, key.substring(1, key.length));
    }
    return get(information, key);
  }

  List<Widget> buildChildren(dynamic information, List data) {
    return data
        .map<List<Widget?>>((e) => buildLayout(
              information,
              e.entries.first.key,
              e.entries.first.value,
            ))
        .fold<List<Widget?>>(<Widget?>[], (p, e) => p..addAll(e))
        .whereType<Widget>()
        .toList();
  }

  Widget? buildChild(dynamic information, Map<String, dynamic>? data) {
    if (data == null) {
      return null;
    }
    return buildLayout(
      information,
      data.entries.first.key,
      data.entries.first.value,
    ).first;
  }

  dynamic methodCheck(dynamic information, String key, dynamic data) {
    final methodRegex = RegExp(r'([a-z]+)?\s?\((.*?)\)');
    if (methodRegex.hasMatch(key)) {
      final match = methodRegex.firstMatch(key)!;
      final name = match.group(1)!;
      final arguments = match.group(2)!;

      final method = methods.firstWhere(
        (element) => element.name == name,
        orElse: () =>
            throw UnsupportedError('No method registered with the name: $name'),
      );

      return method.execute(
        this,
        information,
        arguments.split(RegExp(r'\s?,\s?')),
        data,
      );
    }
    return null;
  }

  List<Widget?> buildLayout(dynamic information, String key, dynamic data) {
    final result = methodCheck(information, key, data);
    if (result != null) {
      return result;
    }

    final widgetConverter = widgets.firstWhere(
      (element) => element.name == key,
      orElse: () =>
          throw UnsupportedError('No widget registered with the name: $key'),
    );

    return [widgetConverter.convert(this, information, data)];
  }

  PdfColor? color(dynamic information, String? input) {
    final hexColor = value(information, input);
    return hexColor != null ? PdfColor.fromHex(hexColor) : null;
  }

  String? value(dynamic information, String? input) {
    if (input == null) {
      return null;
    }
    var output = input;

    // TODO: support multiple operators in order? below regex should do that
    // \${\s?([a-z_.\[\]0-9]*)\s?(?:[|]?\s?([a-z _|]*)){0,}?\s?}
    final matches = RegExp(
      r'\${\s?([a-z_.\[\]0-9]*)\s?(?:[|]?\s?([a-z _]*))?\s?}',
    ).allMatches(input);

    for (final match in matches) {
      final key = match.group(1)!;
      final result = match.group(2)?.split(' ');
      final operator = result?[0];

      var retrievedValue = raw(information, key);
      if (operator != null) {
        if (!['join', 'currency', 'index_of', 'color', 'upper']
            .any((e) => operator == e)) {
          throw UnimplementedError('Operator $operator is not implemented');
        }
        if (operator == 'upper' && retrievedValue is String) {
          retrievedValue = retrievedValue.toUpperCase();
        } else if (operator == 'join' && retrievedValue is List) {
          retrievedValue = retrievedValue.join();
        } else {
          if (operator == 'index_of' && retrievedValue is List) {
            final on = result?[1];
            retrievedValue = retrievedValue.indexOf(raw(information, on)) + 1;
          } else if (operator == 'currency') {
            retrievedValue = NumberFormat.currency(
              locale: Platform.localeName,
              name: rootInformation['valuta'],
              symbol: '',
            ).format(retrievedValue);
          } else if (operator == 'color') {
            if (retrievedValue is String) {
              retrievedValue = PdfColor.fromHex(retrievedValue);
            } else {
              retrievedValue = PdfColor.fromInt(retrievedValue);
            }
            retrievedValue = (retrievedValue as PdfColor).toHex();
          } else {
            throw UnsupportedError(
              'Operator "$operator" cant be applied on "$key" because it is of type ${retrievedValue.runtimeType}',
            );
          }
        }
      }
      output = output.replaceFirst(
        match.group(0)!,
        '$retrievedValue',
      );
    }

    return output;
  }

  List<Widget> build(Context context) {
    this.context = context;
    return buildChildren(rootInformation, _layout);
  }

  List<String> _requiredFields(String key, dynamic data) {
    final fields = <String>[];

    if (data is Map) {
      for (final entry in data.entries) {
        fields.addAll(_requiredFields(
          '${key.isNotEmpty ? '$key.' : ''}${entry.key}',
          entry.value,
        ));
      }
    }
    if (data is List) {
      final results = data.map((e) => _requiredFields('$key[0]', e));
      results.forEach(fields.addAll);
    }
    if (data == null) {
      fields.add(key);
    }

    return fields;
  }
}
