import 'package:okane/template_builder/method.dart';
import 'package:okane/template_builder/template_builder.dart';
import 'package:pdf/widgets.dart';

class IfMethod extends Method {
  @override
  String get name => 'if';

  @override
  String get example => '''
somewhere:
  if (path.to.truthy.value):
    text:
      value: Value was true''';

  @override
  List<Widget?> execute(
    TemplateBuilder template,
    information,
    List<String> arguments,
    layoutData,
  ) {
    final value = template.raw(information, arguments.first);
    if (value == null) {
      return [null];
    }
    if (value is num && value == 0) {
      return [null];
    }
    if ((value is List || value is Map || value is String) && value.isEmpty) {
      return [null];
    }
    return template.buildLayout(
      information,
      layoutData.entries.first.key,
      layoutData.entries.first.value,
    );
  }
}
