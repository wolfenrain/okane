import 'package:okane/template_builder/method.dart';
import 'package:okane/template_builder/template_builder.dart';
import 'package:pdf/widgets.dart';

class ForMethod extends Method {
  @override
  String get name => 'for';

  @override
  String get example => r'''
somewhere:
  for (item in path.to.list):
    text:
      value: Item was ${item}''';

  @override
  List<Widget?> execute(
    TemplateBuilder template,
    information,
    List<String> arguments,
    layoutData,
  ) {
    final forRegex = RegExp(r'([a-z]*) in ([a-z_.]*)');
    final match = forRegex.firstMatch(arguments.first)!;
    final name = match.group(1)!;
    final from = match.group(2)!;
    final value = template.raw(information, from);
    final toBuild = layoutData.entries.first;

    if (value is Map) {
      return [
        for (final entry in value.entries)
          template.buildLayout({
            name: {'key': entry.key, 'value': entry.value}
          }, toBuild.key, toBuild.value).first,
      ].whereType<Widget>().toList();
    } else if (value is List) {
      return value
          .map((e) =>
              template.buildLayout({name: e}, toBuild.key, toBuild.value).first)
          .toList();
    }

    throw UnsupportedError(
      'For loop not supported on ${value.runtimeType} for key $from',
    );
  }
}
