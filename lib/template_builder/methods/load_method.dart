import 'package:okane/template_builder/method.dart';
import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/utils/load_yaml.dart';
import 'package:pdf/widgets.dart';

class LoadMethod extends Method {
  @override
  String get name => 'load';

  @override
  String get example => r'''
somewhere:
  load(./path/to/file.yaml, extra, arguments): ${variable.to.use.as.data}''';

  @override
  List<Widget?> execute(
    TemplateBuilder template,
    information,
    List<String> arguments,
    layoutData,
  ) {
    final parameterRegex = RegExp(r'\${\s?([a-z_.]*)');
    final match = parameterRegex.firstMatch(layoutData ?? '');
    final key = match?.group(1);

    final builder = TemplateBuilder(
      template.uri.resolve(arguments.first),
      openYaml(template.uri.resolve(arguments.first)),
      {
        'arguments': arguments.sublist(1, arguments.length),
        ...template.raw(information, key),
      },
      root: '${template.root}${key == null ? '' : '.$key'}',
    );
    return builder.build(template.context);
  }
}
