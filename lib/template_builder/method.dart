import 'package:okane/template_builder/template_builder.dart';
import 'package:pdf/widgets.dart';

export 'methods/if_method.dart';
export 'methods/for_method.dart';
export 'methods/load_method.dart';

abstract class Method {
  String get name;

  String get example;

  List<Widget?> execute(
    TemplateBuilder template,
    dynamic information,
    List<String> arguments,
    dynamic layoutData,
  );
}
