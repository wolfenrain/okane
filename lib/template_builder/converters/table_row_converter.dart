import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class _TableRow extends Widget {
  _TableRow({required this.children});

  final List<Widget> children;

  @override
  void layout(
    Context context,
    BoxConstraints constraints, {
    bool parentUsesSize = false,
  }) {
    throw Exception('table_row can not be constructed outside a table');
  }
}

class TableRowConverter extends WidgetConverter {
  @override
  String get name => 'table_row';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return _TableRow(
      children: template.buildChildren(information, data!['children']),
    );
  }

  static TableRow? toRow(Widget? widget) {
    if (widget is! _TableRow) {
      return null;
    }
    return TableRow(children: widget.children);
  }
}
