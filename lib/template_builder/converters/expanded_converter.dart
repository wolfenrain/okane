import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class ExpandedConverter extends WidgetConverter {
  @override
  String get name => 'expanded';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    final flex = data!['flex'] ?? 1;

    return Expanded(
      flex: flex,
      child: template.buildChild(information, data['child']) ?? SizedBox(),
    );
  }
}
