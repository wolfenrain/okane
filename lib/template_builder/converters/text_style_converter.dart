import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class TextStyleConverter extends WidgetConverter {
  @override
  String get name => 'text_style';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    final style = TextStyle(
      fontSize: data!['style']?['font_size']?.toDouble(),
      fontWeight: fontWeight(data['style']?['font_weight']),
    );

    return DefaultTextStyle(
      style: style,
      textAlign: textAlign(data['text_align']),
      child: template.buildChild(information, data['child'])!,
    );
  }
}
