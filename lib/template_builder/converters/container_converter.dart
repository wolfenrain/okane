import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class ContainerConverter extends WidgetConverter {
  @override
  String get name => 'container';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return Container(
      width: data!['width']?.toDouble(),
      height: data['height']?.toDouble(),
      decoration: boxDecoration(template, information, data['decoration']),
      child: template.buildChild(information, data['child']),
    );
  }
}
