import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

class TextConverter extends WidgetConverter {
  @override
  String get name => 'text';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    final style = TextStyle(
      fontSize: data!['style']?['font_size']?.toDouble(),
      fontWeight: fontWeight(data['style']?['font_weight']),
      color: template.color(information, data['style']?['color']),
    );

    return Text(
      template.value(information, data['value'].toString())!,
      textAlign: textAlign(data['text_align']),
      style: style,
    );
  }
}
