import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class PaddingConverter extends WidgetConverter {
  @override
  String get name => 'padding';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return Padding(
      padding: EdgeInsets.only(
        top: data!['top']?.toDouble() ?? 0.0,
        left: data['left']?.toDouble() ?? 0.0,
        right: data['right']?.toDouble() ?? 0.0,
        bottom: data['bottom']?.toDouble() ?? 0.0,
      ),
      child: template.buildChild(information, data['child']),
    );
  }
}
