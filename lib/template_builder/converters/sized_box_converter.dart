import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class SizedBoxConverter extends WidgetConverter {
  @override
  String get name => 'sized_box';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return SizedBox(
      width: data!['width']?.toDouble(),
      height: data['height']?.toDouble(),
      child: template.buildChild(information, data['child']),
    );
  }
}
