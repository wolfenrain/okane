import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class DividerConverter extends WidgetConverter {
  @override
  String get name => 'divider';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return Divider(color: template.color(information, data?['color']));
  }
}
