import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class TableConverter extends WidgetConverter {
  @override
  String get name => 'table';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    final columnWidths = <int, TableColumnWidth>{
      for (final column in data!['column_widths'] ?? [])
        ...tableColumnWidth(column),
    };
    final tableRows = <TableRow?>[];
    final children = data['children'] ?? [];

    children.forEach((e) {
      final item = e.entries.first;
      final result = template.methodCheck(information, item.key, item.value);
      if (result != null) {
        tableRows.addAll(result.map<TableRow?>(TableRowConverter.toRow));
      } else {
        if (item.key != 'table_row') {
          throw UnsupportedError('table only supports nested table_row');
        }

        tableRows.add(TableRow(
          children: result ??
              template.buildChildren(information, item.value['children']),
        ));
      }
    });

    return Table(
      border: TableBorder(
        top: borderSide(template, information, data['border']?['top']),
        left: borderSide(template, information, data['border']?['left']),
        right: borderSide(template, information, data['border']?['right']),
        bottom: borderSide(template, information, data['border']?['bottom']),
      ),
      columnWidths: columnWidths,
      children: tableRows.whereType<TableRow>().toList(),
    );
  }
}
