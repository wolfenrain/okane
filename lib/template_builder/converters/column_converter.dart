import 'package:okane/template_builder/template_builder.dart';
import 'package:okane/template_builder/widget_converter.dart';
import 'package:pdf/widgets.dart';

class ColumnConverter extends WidgetConverter {
  @override
  String get name => 'column';

  @override
  Widget? convert(
    TemplateBuilder template,
    information,
    Map<String, dynamic>? data,
  ) {
    return Column(
      mainAxisAlignment: mainAxisAlignment(
        data!['main_axis_alignment'],
      ),
      crossAxisAlignment: crossAxisAlignment(
        data['cross_axis_alignment'],
      ),
      children: template.buildChildren(information, data['children']),
    );
  }
}
