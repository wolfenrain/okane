import 'dart:io';

import 'package:okane/utils/get.dart';

void requiredFields(List<String> fields, dynamic data,
    {required String message}) {
  final missingFields = fields.where((f) => get(data, f) == null);
  if (missingFields.isNotEmpty) {
    print([message, for (final field in missingFields) '- .$field'].join('\n'));
    exit(1);
  }
}
