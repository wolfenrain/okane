import 'dart:convert';
import 'dart:io';

import 'package:yaml/yaml.dart';

Map<String, dynamic> _convert(YamlMap m) => json.decode(json.encode(m));

dynamic openYaml(Uri uri) {
  try {
    print('Opening $uri');
    return _convert(loadYaml(
      File.fromUri(uri).readAsStringSync(),
    ));
  } on FileSystemException catch (err) {
    print('Could not open ${err.path}, check if it is the correct path.');
    exit(1);
  }
}
