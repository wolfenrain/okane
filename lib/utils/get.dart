final _reEscapeChar = RegExp(r'\\(\\)?');
final _rePropName = RegExp(
  [
    // Match anything that isn't a dot or bracket.
    '[^.[\\]]+', '|',
    // Or match property names within brackets.
    '\\[(?:',
    // Match a non-string expression.
    '([^"\'][^[]*)',
    '|',
    // Or match strings (supports escaping characters).
    '(["\'])((?:(?!\\2)[^\\\\]|\\\\.)*?)\\2',
    r')\\]',
    '|',
    // Or match "" as the space between consecutive dots or empty brackets.
    r'(?=(?:\\.|\\[\\])(?:\\.|\\[\\]|$))',
  ].join(''),
);

List<String> _stringToPath(String input) {
  return [
    if (input[0] == '.') '',
    ..._rePropName.allMatches(input).map((match) {
      var key = match.group(0)!;
      final expression = match.group(1);
      final quote = match.group(2);
      final subString = match.input;

      if (quote != null) {
        key = subString.replaceAll(_reEscapeChar, r'$1');
      } else if (expression != null) {
        key = expression.trim();
      }
      return key;
    }),
  ];
}

T? get<T>(dynamic obj, String path) {
  if (obj is! Map && obj is! List) return obj;

  final fragments = _stringToPath(path);
  var index = -1;

  while (obj != null && ++index < fragments.length) {
    final key = int.tryParse(fragments[index]) ?? fragments[index];
    if (obj is List && key is! int) {
      return null;
    }
    obj = obj[key];
  }

  return obj;
}
