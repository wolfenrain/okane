import 'dart:io';

import 'package:okane/utils/get.dart';
import 'package:okane/utils/load_yaml.dart';

class Config {
  Config(this.path) : source = openYaml(path);

  final Uri path;

  final Map<String, dynamic> source;

  String value(String path) => get(source, path);

  Uri get templateDir => path.resolve('${value('templates_dir')}/');

  List<String> get templates {
    return Directory.fromUri(templateDir)
        .listSync()
        .whereType<File>()
        .where((f) => f.path.endsWith('.yaml'))
        .map((e) => e.uri.pathSegments.last.split('.').first)
        .toList();
  }
}
