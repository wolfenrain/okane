# `okane`

`okane` is an unnecessarily complicated and over-engineered invoice generator.

## How it works

At the base of it all you have a `okane.yaml` file. This file contains your global configurations.

In your `okane.yaml` you define where your templates live, templates are YAML files that define how the PDF 
will be generated and what data it requires.

And finally you have input files. These files are read by `okane` to generate a PDF file based on the given template.

```bash
dart compile exe ./bin/okane.dart -o ./example/okane
cd ./example
./okane generate input/example_invoice.yaml invoices/example.pdf
./okane generate input/example_quote.yaml quotes/example.pdf
```

You can find the output here:
- [Invoice example](https://gitlab.com/wolfenrain/okane/-/blob/main/example/invoices/example.pdf)
- [Quote example](https://gitlab.com/wolfenrain/okane/-/blob/main/example/quotes/example.pdf)

## Document references

For more information about document syntax, methods and widgets see the [Okane document reference](https://gitlab.com/wolfenrain/okane/-/blob/main/okane_document_reference.md)
