# Okane Document Reference

### okane.yaml

```yaml
templates_dir: ./templates # Directory where the templates are

defaults:
  <template_name>:
    # Default fields for specific template can be set like this.

# Fields on the root level will be applied to all templates.
<field_name>: <field_value>

theme:
  # While not "standard", templates often use the theme field for theming data.
```

### <template_file>.yaml

A template file is used for defining the structure. You can split up your template into partial files.
A partial file is the same as a template file but it can be reused. By using the `load` method you can 
load your partials where you need them.

```yaml
layout: # Each template file requires the layout field, and it should always be an array.
- # Your layout.

# Any required field for this template can be defined like this:
<field_name>: null # Setting it to null will tell okane that it is a required field

# Optional fields don't have to be defined but to make it easy you can add them commented out:
# <field_name>: optional
```

**NOTE**: Only the root level files in your template directory will be considered template files, so any partials you 
might have should be placed inside a nested directory.

### <input_file>.yaml

```yaml
okane:
  template: <template_name> # Optional template, if missing the "--template" parameter is required on the command line.

# Fields required for the template are on the root level.
<field_name>: <field_value>
```

    
## Methods

The following methods are supported by `okane`:

The `if` method: 

```yaml
somewhere:
  if (path.to.truthy.value):
    text:
      value: Value was true
```

The `for` method: 

```yaml
somewhere:
  for (item in path.to.list):
    text:
      value: Item was ${item}
```

The `load` method: 

```yaml
somewhere:
  load(./path/to/file.yaml, extra, arguments): ${variable.to.use.as.data}
```

## Widgets

The following widgets are supported by `okane`:

The `column` widget: 

```yaml
somewhere:
  column:
```

The `container` widget: 

```yaml
somewhere:
  container:
```

The `divider` widget: 

```yaml
somewhere:
  divider:
```

The `expanded` widget: 

```yaml
somewhere:
  expanded:
```

The `expanded` widget: 

```yaml
somewhere:
  expanded:
```

The `padding` widget: 

```yaml
somewhere:
  padding:
```

The `row` widget: 

```yaml
somewhere:
  row:
```

The `sized_box` widget: 

```yaml
somewhere:
  sized_box:
```

The `table` widget: 

```yaml
somewhere:
  table:
```

The `table_row` widget: 

```yaml
somewhere:
  table_row:
```

The `text` widget: 

```yaml
somewhere:
  text:
```

The `text_style` widget: 

```yaml
somewhere:
  text_style:
```
